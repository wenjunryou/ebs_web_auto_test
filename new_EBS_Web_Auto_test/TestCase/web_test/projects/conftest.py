import time

import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager

from Lib.parseConFile import ReadConfig, load_config
from Page.login_page import LoginPage
from Config.config import CONF_PATH, PROJECT_CONF_PATH
from Page.projects_page import ProjectsPage

driver = None
@pytest.fixture(scope='session', autouse=False)
def drivers(request):
    global driver
    # service = ChromeService(executable_path=ChromeDriverManager().install())
    # driver = webdriver.Chrome(service=service)
    option = webdriver.ChromeOptions()
    option.add_argument('no-sandbox')
    driver = webdriver.Chrome(options=option)
    driver.maximize_window()

    def fn():
        driver.quit()
    request.addfinalizer(fn)
    return driver

@pytest.fixture(scope='session', autouse=False)
def user_login(drivers):
    global config
    config = ReadConfig(CONF_PATH)
    login = LoginPage(drivers)
    login.login(config.BASIC_URL, config.NAME, config.PASSWORD)


@pytest.fixture(scope='function', autouse=False)
def config(drivers):
    return config

@pytest.fixture(scope='function', autouse=False)
def add_project(drivers, user_login, gen_project_name, project_name, request):
    project = ProjectsPage(drivers)
    project.add_project(project_name)
    time.sleep(3)
    def fn():
        project.delete_projects(project_name)
    request.addfinalizer(fn)

@pytest.fixture(scope='function', autouse=False)
def add_package(drivers, user_login, config_list):
    project = ProjectsPage(drivers)
    package_name = config_list['package_name']
    git_url = config_list['git_url']
    package_description = config_list['package_description']
    branch = config_list['branch']
    project.add_package(package_name, git_url, package_description, branch)
    project.load_url(config.BASIC_URL)

@pytest.fixture(scope='function', autouse=False)
def delete_projects(drivers, project_name, request):
    def fn():
        project = ProjectsPage(drivers)
        time.sleep(5)
        project.load_url(config.BASIC_URL)
        project.delete_projects(project_name)
    request.addfinalizer(fn)
@pytest.fixture(scope='function', autouse=False)
def gen_project_name():
    global project_name
    config_list = load_config(PROJECT_CONF_PATH)
    project_name = config_list['pre_project_name'] + str(time.time_ns())

@pytest.fixture(scope='function', autouse=False)
def project_name():
    return project_name

@pytest.fixture(scope='session', autouse=False)
def config_list():
    config_list = load_config(PROJECT_CONF_PATH)
    return config_list


@pytest.fixture(scope='function', autouse=False)
def go_homepage(drivers, request):
    def fn():
        project = ProjectsPage(drivers)
        project.load_url(config.BASIC_URL)
    request.addfinalizer(fn)

