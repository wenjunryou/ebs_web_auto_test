# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/17 0022 14:15
@Auth ： wenjun
@File ：test_case_projects_01.py
@IDE ：PyCharm
"""
import time
import pytest
from Lib.log import log
from Lib.readelement import Element
from Page.projects_page import ProjectsPage


class TestProjectsCase:

    def test_query_private_project_01(self, drivers, user_login, add_project, project_name, go_homepage):
        log.info('对私有工程进行模糊查询')
        project = ProjectsPage(drivers)
        project.search_project_name(project_name)
        project_list = project.get_projects_list()
        assert project_name in project_list
        log.info('搜索框搜索成功！')

    def test_add_private_project_01(self, drivers, user_login, gen_project_name, project_name, delete_projects,
                                    go_homepage):
        log.info('创建私有工程－描述为空')
        project = ProjectsPage(drivers)
        project.add_project(project_name)
        project.search_project_name(project_name)
        project_list = project.get_projects_list()
        assert project_name in project_list
        log.info('创建私有工程－描述为空 成功！')

    def test_add_private_project_02(self, drivers, user_login, gen_project_name, project_name, delete_projects,
                                    go_homepage):
        log.info('创建私有工程－分支为空')
        project = ProjectsPage(drivers)
        project.add_project(project_name, branch=' ')
        project.search_project_name(project_name)
        project_list = project.get_projects_list()
        assert project_name in project_list
        log.info('创建私有工程－分支为空 成功！')

    def test_add_private_project_03(self, drivers, user_login, gen_project_name, project_name, delete_projects,
                                    config_list, go_homepage):
        log.info('创建私有工程－描述长度超过最大值')
        project = ProjectsPage(drivers)
        length = config_list['description_len_max']
        description = project.generate_random_string(length+1)
        project.add_project(project_name, description=description)
        project.search_project_name(project_name)
        actual_description = project.get_first_description()
        assert actual_description == description[:length]
        project_list = project.get_projects_list()
        assert project_name in project_list
        log.info('创建私有工程成功！')

    def test_add_private_project_04(self, drivers, user_login, gen_project_name, project_name, delete_projects,
                                    config_list, go_homepage):
        log.info('创建私有工程－描述长度非空')
        project = ProjectsPage(drivers)
        description = config_list['description']
        project.add_project(project_name, description=description)
        project.search_project_name(project_name)
        actual_description = project.get_first_description()
        assert actual_description == config_list['description']
        project_list = project.get_projects_list()
        assert project_name in project_list
        log.info('创建私有工程成功！')

    def test_add_private_project_05(self, drivers, user_login, config_list, go_homepage):
        log.info('创建私有工程－工程名为空')
        project = ProjectsPage(drivers)
        project.add_project('')
        error_message = project.get_error_message()
        assert error_message == config_list['project_name_not_null']

    def test_add_private_project_06(self, drivers, user_login, config_list, go_homepage):
        log.info('创建私有工程－工程名长度低于最小限制')
        project = ProjectsPage(drivers)
        project_name = project.generate_random_string(config_list['project_name_len_min']-1)
        project.add_project(project_name)
        error_message = project.get_error_message()
        assert error_message == config_list['project_name_len_check']

    def test_add_private_project_07(self, drivers, user_login, config_list, go_homepage):
        log.info('创建私有工程－工程名长度超过最大限制')
        project = ProjectsPage(drivers)
        project_name = project.generate_random_string(config_list['project_name_len_max']+1)
        project.add_project(project_name)
        error_message = project.get_error_message()
        assert error_message == config_list['project_name_len_check']

    def test_add_private_project_08(self, drivers, user_login, config_list, go_homepage):
        log.info('创建私有工程－工程名中包含中文字符')
        project = ProjectsPage(drivers)
        project_name = config_list['project_name_CN']
        project.add_project(project_name)
        error_message = project.get_error_message()
        assert error_message == config_list['project_name_con_check']

    def test_check_private_project_overview_01(self, drivers, user_login, add_project, project_name, go_homepage):
        log.info('私有工程概览页面信息查询')
        project = ProjectsPage(drivers)
        project.search_project_name(project_name)
        project.enter_first_project()
        flag = project.check_private_project_overview()
        assert flag == True

    def test_check_private_project_overview_02(self, drivers, user_login, add_project, add_package, project_name,
                                               config_list, go_homepage):
        log.info('私有工程概览页面对存在的软件包查询')
        project = ProjectsPage(drivers)
        project.enter_project_view()
        project.enter_private_projects()
        project.search_project_name(project_name)
        project.enter_first_project()
        time.sleep(5)
        project.search_package_name(config_list['package_name'])
        package_name_list = project.get_package_list()
        for pak in package_name_list:
            if not pak.__contains__(config_list['package_name']):
                assert 1 == 2
        assert 1 == 1

    def test_check_private_project_overview_03(self, drivers, user_login, add_project, add_package, project_name,
                                               config_list, go_homepage):
        log.info('私有工程概览页面对不存在的软件包查询')
        project = ProjectsPage(drivers)
        project.enter_project_view()
        project.enter_private_projects()
        project.search_project_name(project_name)
        project.enter_first_project()
        time.sleep(5)
        project.search_package_name(config_list['package_name'] + 's')
        pro = Element('project')
        flag = project.is_element_exist(pro['package_list'])
        assert flag == False

    def test_check_private_project_overview_04(self, drivers, user_login, add_project, project_name, config_list,
                                               go_homepage):
        log.info('私有工程概览页面添加软件包－package name必填校验')
        project = ProjectsPage(drivers)
        project.search_project_name(project_name)
        package_name = ''
        git_url = config_list['git_url']
        package_description = config_list['package_description']
        branch = config_list['branch']
        project.add_package(package_name, git_url, package_description, branch)
        error_message = project.get_error_message()
        assert error_message == config_list['package_name_not_null']

    def test_check_private_project_overview_05(self, drivers, user_login, add_project, project_name, config_list,
                                               go_homepage):
        log.info('私有工程概览页面添加软件包－package name超过最大长度校验')
        project = ProjectsPage(drivers)
        project.search_project_name(project_name)
        package_name = project.generate_random_string(config_list['package_name_len_max'] + 1)
        git_url = config_list['git_url']
        package_description = config_list['package_description']
        branch = config_list['branch']
        project.add_package(package_name, git_url, package_description, branch)
        error_message = project.get_error_message()
        assert error_message == config_list['package_name_len_check']

    def test_check_private_project_overview_06(self, drivers, user_login, add_project, project_name, config_list,
                                               go_homepage):
        log.info('私有工程概览页面添加软件包－url必填校验')
        project = ProjectsPage(drivers)
        project.search_project_name(project_name)
        package_name = config_list['package_name']
        git_url = ''
        package_description = config_list['package_description']
        branch = config_list['branch']
        project.add_package(package_name, git_url, package_description, branch)
        error_message = project.get_error_message()
        assert error_message == config_list['git_url_not_null']

    def test_check_private_project_overview_07(self, drivers, user_login, add_project, project_name, config_list,
                                               go_homepage):
        log.info('私有工程概览页面添加软件包－url格式校验')
        project = ProjectsPage(drivers)
        project.search_project_name(project_name)
        package_name = config_list['package_name']
        git_url = config_list['git_url_format_error']
        package_description = config_list['package_description']
        branch = config_list['branch']
        project.add_package(package_name, git_url, package_description, branch)
        error_message = project.get_error_message()
        assert error_message == config_list['git_url_format_check']

    def test_check_private_project_overview_08(self, drivers, user_login, add_project, project_name, config_list,
                                               go_homepage):
        log.info('私有工程概览页面添加软件包－branch超过最大长度校验')
        project = ProjectsPage(drivers)
        project.search_project_name(project_name)
        package_name = config_list['package_name']
        git_url = config_list['git_url']
        package_description = config_list['package_description']
        branch = project.generate_random_string(config_list['branch_len_max'] + 1)
        project.add_package(package_name, git_url, package_description, branch)
        error_message = project.get_error_message()
        assert error_message == config_list['branch_len_check']

    def test_check_private_project_overview_09(self, drivers, user_login, add_project, add_package, project_name,
                                               config_list, go_homepage):
        log.info('私有工程概览页面继承项目按钮功能检查')
        project = ProjectsPage(drivers)
        project.enter_project_view()
        project.enter_private_projects()
        project.search_project_name(project_name)
        project.enter_first_project()
        time.sleep(5)
        project.search_package_name(config_list['package_name'])
        project.enter_first_package()
        project.enter_branch_package()
        pro = Element('project')
        flag = project.is_element_exist(pro['branch_package_dialog'])
        assert flag == True

    def test_check_private_project_overview_10(self, drivers, user_login, add_project, add_package, project_name,
                                               config_list, go_homepage):
        log.info('私有工程概览页面继承项目弹窗页面信息检查')
        project = ProjectsPage(drivers)
        project.enter_project_view()
        project.enter_private_projects()
        project.search_project_name(project_name)
        project.enter_first_project()
        time.sleep(5)
        project.search_package_name(config_list['package_name'])
        project.enter_first_package()
        project.enter_branch_package()
        flag = project.branch_package_dialog_check()
        assert flag == True

    def test_check_private_project_overview_11(self, drivers, user_login, add_project, add_package, project_name,
                                               config_list, config, go_homepage):
        log.info('私有工程概览页面继承项目弹窗页面确认按钮功能检查')
        project = ProjectsPage(drivers)
        project.enter_project_view()
        project.enter_private_projects()
        project.search_project_name(project_name)
        project.enter_first_project()
        time.sleep(5)
        project.search_package_name(config_list['package_name'])
        project.enter_first_package()
        project.enter_branch_package()
        new_project_name = project_name + '_new'
        project.modify_branch_project_name(new_project_name)
        confirm_message = project.get_confirm_message()
        assert confirm_message == config_list['success']
        expect_current_url = config.BASIC_URL + config_list['overview_url'] + new_project_name
        assert expect_current_url == project.driver.current_url

    def test_check_private_project_overview_12(self, drivers, user_login, add_project, add_package, project_name,
                                               config_list, go_homepage):
        log.info('私有工程概览页面继承项目弹窗页面继承目标工程名为已存在的工程名')
        project = ProjectsPage(drivers)
        project.enter_project_view()
        project.enter_private_projects()
        project.search_project_name(project_name)
        project.enter_first_project()
        time.sleep(5)
        project.search_package_name(config_list['package_name'])
        project.enter_first_package()
        project.enter_branch_package()
        project.modify_branch_project_name(project_name)
        confirm_message = project.get_confirm_message()
        assert confirm_message == config_list['project_name_exist']

    def test_check_private_project_overview_13(self, drivers, user_login, add_project, project_name,
                                               config_list, go_homepage):
        log.info('私有工程概览页面未添加软件包点击全量构建')
        project = ProjectsPage(drivers)
        project.search_project_name(project_name)
        project.enter_first_project()
        time.sleep(3)
        project.full_build()
        confirm_message = project.get_confirm_message()
        assert confirm_message == config_list['invalid_build_target']

    def test_check_private_project_overview_14(self, drivers, user_login, add_project, project_name,
                                               config_list, go_homepage):
        log.info('私有工程概览页面未添加软件包点击增量构建')
        project = ProjectsPage(drivers)
        project.search_project_name(project_name)
        project.enter_first_project()
        time.sleep(3)
        project.incremental_build()
        confirm_message = project.get_confirm_message()
        assert confirm_message == config_list['invalid_build_target']

    def test_check_private_project_overview_15(self, drivers, user_login, add_project, project_name,
                                               config_list, go_homepage):
        log.info('私有工程概览页面未添加软件包点击增量构建')
        project = ProjectsPage(drivers)
        project.search_project_name(project_name)
        project.enter_first_project()
        time.sleep(3)
        project.unlock()
        flag = project.lock_check()
        assert flag == True
        project.lock()









