import pytest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
driver = None
@pytest.fixture(scope='session', autouse=True)
def drivers(request):
    global driver
    # service = ChromeService(executable_path=ChromeDriverManager().install())
    # driver = webdriver.Chrome(service=service)
    option = webdriver.ChromeOptions()
    option.add_argument('no-sandbox')
    driver = webdriver.Chrome(options=option)
    driver.maximize_window()

    def fn():
        driver.quit()
    request.addfinalizer(fn)
    return driver
