# # -*- coding: utf-8 -*-
# """
# @Time ： 2023/5/8 0008 9:47
# @Auth ： ysc
# @File ：run.py
# @IDE ：PyCharm
# """
# import string
# import unittest
# import random
# # from BeautifulReport import BeautifulReport
# from Config.config import REPORT_DIR, HTML_NAME, PRO, PROJECT_CONF_PATH
# from Lib.parseConFile import load_config
# # config_list = load_config(PROJECT_CONF_PATH)
# # print(config_list['pre_project_name'])
# from Lib.readelement import Element
# from Lib.log import log
#
# project = Element('project')
# eles_list = ['overview','build','config','user','inherit_project','full_build','incremental_build','unlock',
#                          'delete','add_package','package_name','changed','refresh','arch','status','total']
# for ele in eles_list:
#     print(ele)
#     print(project[ele])
#
#
# print(project['first_project_name'])
#
# # if __name__ == '__main__':
#     # # 加载目录下所有用例模块
#     # case_path = "TestCase/smoke_test/Login"
#     #
#     # # start_dir是用例模块的路径，pattern是模块名
#     # discover = unittest.defaultTestLoader.discover(start_dir=case_path, pattern="test_case*.py")
#     #
#     # # 实例化一个运行器
#     # br = BeautifulReport(discover)
#     # br.report(filename=HTML_NAME, description="{}_测试报告".format(PRO), report_dir=REPORT_DIR, theme="theme_candy")
import logging

import requests

HEADERS = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/115.0.0.0 Safari/537.36'
}
NOT_FOUND = 401
TOKEN = '9ad768daa5e23b65f5c8b165d5177fc9'

def create_issue(owner,sig_name,maintainer):
    """
    create issue
    """
    issue_name = """【openEuler-20.03-LTS-SP1 update20231101】【arm/x86】docker镜像{0}软件包默认安装版本高于repo中的版本""".format(sig_name)
    body = """【标题描述】docker镜像当前包默认安装版本高于repo中的版本
【环境信息】
软件信息： OS版本及分支：openEuler 22.03 (LTS-SP1)
【问题复现步骤】
对比当前软件包默认安装版本和repo中的版本
【预期结果】
当前软件包默认安装版本低于repo中的版本
【实际结果】
当前软件包默认安装版本高于repo中的版本
【附件信息】
比如系统message日志/组件日志、dump信息、图片等"""
    url = 'https://gitee.com/api/v5/repos/' + owner + '/issues'
    data = {'access_token': TOKEN,
            'owner': owner,
            'repo': sig_name,
            'issue_type': '缺陷',
            'title': issue_name,
            'body': body,
            'assignee': maintainer
            }
    response = requests.post(url,data=data, verify=False)
    print(url,data)
    print(response.text)
    return response.status_code
# import urllib3
# urllib3.disable_warnings()
# logging.captureWarnings(True)
sig_name='workflow_test'
maintainer=''
code = create_issue('wenjunryou', sig_name, maintainer)
print(code)
if code == 201:
    print(sig_name + '提单成功')
else:
    print(sig_name + '提单出错')