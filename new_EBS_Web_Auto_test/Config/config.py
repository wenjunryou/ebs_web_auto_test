# -*- coding: utf-8 -*-
import time
import os
from selenium.webdriver.common.by import By

# 项目名称
PRO = "EulerMaker"

# 项目根目录
ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# 页面元素目录
ELEMENT_PATH = os.path.join(ROOT_DIR, 'Page_element')
# 报告目录
REPORT_DIR = os.path.join(ROOT_DIR, 'Report')
if not os.path.exists(REPORT_DIR): os.mkdir(REPORT_DIR)
# ui对象库config.json文件所在目录
CONF_PATH = os.path.join(ROOT_DIR, 'Config', 'config.json')
PROJECT_CONF_PATH = os.path.join(ROOT_DIR, 'Config', 'project_config.json')
# 当前时间
CURRENT_TIME = time.asctime(time.localtime(time.time()))
REPORT_TIME = time.strftime('%Y-%m-%d-%Hh-%Mm-%Ss', time.localtime())

# 报告名称
HTML_NAME = '{}_Test_Report_{}.html'.format(PRO, REPORT_TIME)
# print(HTML_NAME)

# 元素定位的类型
LOCATE_MODE = {
    'css': By.CSS_SELECTOR,
    'xpath': By.XPATH,
    'name': By.NAME,
    'id': By.ID,
    'class': By.CLASS_NAME
}



