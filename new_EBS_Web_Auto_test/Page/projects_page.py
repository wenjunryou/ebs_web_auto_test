# -*- coding: utf-8 -*-
"""
@Time ： 2023/10/11 0022 17:15
@Auth ： wenjun
@File ：projects_page.py
@IDE ：PyCharm
"""

import time
from selenium.webdriver import Keys

from Page.base_page import BasePage
from Lib.readelement import Element
from Lib.log import log

project = Element('project')

class ProjectsPage(BasePage):
    def enter_add_project(self):
        """
        进入添加工程
        """
        self.click(project['add_project'])

    def enter_project_view(self):
        """
        进入工程查看
        """
        self.click(project['project_view'])

    def enter_private_projects(self):
        """
        进入私有工程页面
        """
        self.click(project['private_projects'])

    def add_project(self, project_name, description='', branch='master'):
        """
        添加工程功能
        """
        try:
            self.enter_project_view()
            self.enter_private_projects()
            self.enter_add_project()
            log.info('start add project ' + project_name)
            time.sleep(2)
            self.send_keys(project['project_name_con'], project_name)
            self.send_keys(project['description_con'], description)
            if branch != "master":
                branch_input = self.find_element(project['branch_con'])
                branch_input.clear()
                branch_input.send_keys(branch)
            self.click(project['confirm'])
        except Exception as e:
            log.error("error: add project failure!")
            log.info(e)

    def enter_add_package(self):
        """
        进入添加软件包
        """
        self.click(project['add_package'])

    def add_package(self, package_name, git_url, description="", branch=""):
        """
        添加工程功能
        """
        try:
            time.sleep(3)
            self.enter_first_project()
            time.sleep(3)
            self.enter_add_package()
            log.info('start add package ' + package_name)
            time.sleep(2)
            self.send_keys(project['package_name_con'], package_name)
            self.send_keys(project['package_description_con'], description)
            self.send_keys(project['git_url_con'], git_url)
            self.send_keys(project['package_branch_con'], branch)
            self.click(project['confirm'])
            time.sleep(3)
        except Exception as e:
            log.error("error: add package failure!")
            log.info(e)

    def search_package_name(self, package_name):
        """
        查询project
        """
        try:
            search_package = self.find_element(project['search_package_name'])
            search_package.clear()
            search_package.send_keys(package_name)
            search_package.send_keys(Keys.ENTER)
        except Exception as e:
            log.error("error: search package failure!", e)

    def get_first_package(self):
        """
         获取第一个软件包名称
        """
        try:
            return self.get_element_text(project['first_package_name'])
        except Exception as e:
            log.error("enter first package name failure!", e)

    def get_package_list(self):
        """
         获取页面私有工程表中的project name
        """
        try:
            package_name_list, package_list = [], []
            package_list = self.find_elements(project['package_list'])
            for pak in package_list:
                package_name_list.append(pak.text)
            return package_name_list
        except Exception as e:
            log.error("get package list failure!", e)

    def search_project_name(self, project_name):
        """
        查询project
        """
        try:
            search_project = self.find_element(project['search_project_name'])
            search_project.clear()
            search_project.send_keys(project_name)
            search_project.send_keys(Keys.ENTER)
        except Exception as e:
            log.error("error: search project failure!", e)

    def get_projects_list(self):
        """
         获取页面私有工程表中的project name
        """
        try:
            project_name_list = []
            projects_list = self.find_elements(project['projects_list'])
            for row in projects_list:
                project_name_list.append(row.text.split()[0])
            return project_name_list
        except Exception as e:
            log.error("get projects list failure!", e)
    def get_first_description(self):
        """
         获取页面私有工程表中的第一行中的description
        """
        try:
            description = self.find_element(project['first_description_name'])
            return description.text
        except Exception as e:
            log.error("get description failure!", e)

    def enter_first_project(self):
        """
         进入第一个工程
        """
        try:
            self.click(project['first_project_name'])
        except Exception as e:
            log.error("enter first project failure!", e)

    def full_build(self):
        """
         点击全量构建
        """
        try:
            self.click(project['full_build'])
        except Exception as e:
            log.error("click full build failure!", e)

    def incremental_build(self):
        """
         点击增量构建
        """
        try:
            self.click(project['incremental_build'])
        except Exception as e:
            log.error("click incremental build failure!", e)

    def lock(self):
        """
         点击锁定
        """
        try:
            self.click(project['lock'])
        except Exception as e:
            log.error("click lock failure!", e)

    def unlock(self):
        """
         点击解锁
        """
        try:
            self.click(project['unlock'])
        except Exception as e:
            log.error("click unlock failure!", e)

    def lock_check(self):
        """
         锁定按钮检查
        """
        try:
            eles_list = ['lock']
            flag = self.check_elements(eles_list)
        except Exception as e:
            log.error("button lock check failure!", e)
        return flag
    def enter_first_package(self):
        """
         进入第一个软件包
        """
        try:
            self.click(project['first_package_name'])
        except Exception as e:
            log.error("enter first package failure!", e)

    def enter_branch_package(self):
        """
         进入软件包继承页面
        """
        try:
            self.click(project['branch_package'])
        except Exception as e:
            log.error("enter branch package failure!", e)

    def expand_more_options(self):
        """
         软件包继承弹窗页面展开更多操作
        """
        try:
            self.click(project['more_options'])
        except Exception as e:
            log.error("expand more options failure!", e)
    def branch_package_dialog_check(self):
        """
         软件包继承弹窗页面检查
        """
        try:
            self.expand_more_options()
            eles_list = ['branch_project_name', 'confirm', 'cancel', 'close']
            flag = self.check_elements(eles_list)
        except Exception as e:
            log.error("branch package dialog check failure!", e)
        return flag

    def modify_branch_project_name(self, new_project_name):
        """
         修改继承目标工程名
        """
        try:
            self.expand_more_options()
            time.sleep(5)
            self.send_keys(project['branch_project_name'], new_project_name)
            self.click(project['confirm'])
        except Exception as e:
            log.error("modify branch project name failure!", e)


    def check_private_project_overview(self):
        """
         私有工程概览页面信息查询
        """
        try:
            eles_list = ['overview','build','config','user','inherit_project','full_build','incremental_build','unlock',
                         'delete','add_package','package_name','changed','refresh','arch','status','total']
            flag = self.check_elements(eles_list)
        except Exception as e:
            log.error("private project overview check failure!", e)
        return flag

    def check_elements(self, eles_list):
        """
         私有工程概览页面信息查询
        """
        try:
            flag = True
            for ele in eles_list:
                log.info('check element ' + ele)
                flag = self.is_element_exist(project[ele])
                if not flag:
                    break
            return flag
        except Exception as e:
            log.error("private project overview check failure!", e)
    def delete_single_project(self):
        """
         根据工程名删除单个工程
        """
        try:
            self.click(project['first_project_name'])
            time.sleep(3)
            self.click(project['delete'])
            time.sleep(2)
            self.click(project['confirm'])
            time.sleep(2)
        except Exception as e:
            log.error("delete single project failure!", e)

    def delete_projects(self, project_name):
        """
         根据工程名删除工程
        """
        try:
            log.info('start delete project ' + project_name)
            self.enter_project_view()
            self.enter_private_projects()
            self.search_project_name(project_name)
            project_name_list = self.get_projects_list()
            i = 1
            for project_name in project_name_list:
                if i >= 2:
                    self.enter_project_view()
                    self.enter_private_projects()
                    self.search_project_name(project_name)
                self.delete_single_project()
                i += 1
        except Exception as e:
            log.error("delete projects failure!", e)

    def get_error_message(self):
        """
        获取错误提示信息
        """
        try:
            error_message = self.get_element_text(project['error_message'])
            return error_message
        except Exception as e:
            log.error("get error message failure!", e)

    def get_confirm_message(self):
        """
        获取提交之后提示信息
        """
        try:
            time.sleep(3)
            confirm_message = self.get_element_text(project['confirm_message'])
            return confirm_message
        except Exception as e:
            log.error("get confirm message failure!", e)




