from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager

def getDriver():
    # service = ChromeService(executable_path=ChromeDriverManager().install())
    # driver = webdriver.Chrome(service=service)
    option = webdriver.ChromeOptions()
    option.add_argument('no-sandbox')
    driver = webdriver.Chrome(options=option)
    return driver
